# Reactimal

Reactimal provides a set of primitives that can be used to express your code as a logic graph using
reactive programming methods.

Reactive programming inverts the dependency relationship between
functions and their inputs. No longer do callers need to know the signature of the functions
they call. Instead computed values subscribe to scalar properties or other computed values. The
reactive programming method is ideal for state that changes as a result of external inputs. It also
improves code readability and reduces the tendency toward spaghetti code.

Reactimal provides the following reactive programming primitives. Each type of primitive is
[subscribable](./docs/subscribable.md) and provides a single output value (or promise), but differ
in how they receive their input.

## Input Primitives

- A [property](./docs/property.md) directly receives a scalar value. It serves as an entry point to
  a logic graph for scalar values.
- A [container](./docs/container.md) wraps javascript container objects and triggers an update when
  a property changes or a method is called. (NOTE: Nestable container changes are tracked only one
  level deep.)

## Computational Primitives

- A [computed](./docs/computed.md) only receives values from other subscribables but serves to
  compute new values. This is the function in the logic graph.
- A [stream](./docs/stream.md) is the asynchronous version of a computed.

Reactimal also includes a few [utilities](./docs/utilities.md) that may be useful when building
logic graphs.

## Inspiration

Reactimal is the result of years of learning from the following projects:

- [KnockoutJS](http://knockoutjs.com/)
- [Overture](https://github.com/fastmail/overture)
- [Redux](https://redux.js.org/)
- [Mithril](https://mithril.js.org/)
