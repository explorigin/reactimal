# container

`container` is a [subscribable](./subscribable.md)
[object proxy](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Proxy)
around any container type (Object, Set, Map, or Array) that will propagate the object to any
subscribers whenever it changes.

## Behavior

Anytime a property is set or a method is called, the container will trigger subscribers. Unlike
other subscribables in Reactimal, the identity operator is not sufficient to detect a noteworthy
change. To limit propagated changes, build a `hashableContainer` and provide a hash function to
determine if the proxied container has changed.

## Usage

### Creation

```js
const monkeys = hashableContainer(arr => arr.join('$'))([]);
const firstMonkey = computed(m => (m.length ? m[0] : null), [monkeys]);
firstMonkey.subscribe(console.log.bind(console));
```

### Add a member to the container

```js
monkeys.push('Bill');
```

_firstMonkey_ would be computed and "Bill" would be logged to the console.

### Access the contained object directly

Reference the `_` (underscore) property to access the contained object directly.

```js
monkeys._.push('Bill');
```

The array in _monkeys_ would get a new value without _firstMonkey_ being notified of the change.
