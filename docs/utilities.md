# Utilities

Reactimal provides a few utility functions that may be useful when working with its primitives.

## id

`id(anything) -> anything`

`id` is a function that returns its first parameter. It is used as the default hash function for
each [subscribable](./subscribable.md).

## call

`call(anything) -> void`

`call` will call its first parameter if it is a function otherwise return the parameter. It is used
for iterating over dependent inputs of [computeds](./docs/computed.md) and
[streams](./docs/stream.md).

## pick

`pick(propName, default) -> (obj) -> any`

`pick` returns a function that accepts an object and will return the object's property for the
provided name or the default if one is supplied. `pick` is not directly used with any
[subscribable](./subscribable.md) but can be useful as the [computed](./computed.md) function when
breaking down a `prop` that contains an object or a container.
