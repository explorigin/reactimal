export const id = a => a;

export const pick = (id, def) => doc => (doc && doc.hasOwnProperty(id) ? doc[id] : def);

export const call = a => (typeof a === 'function' ? a() : a);
