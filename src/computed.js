import { id, call } from './util.js';
import { subscribable } from './subscribable.js';


export const hashableComputed = hash => (fn, dependencies = []) => {
	let isDirty = true;
	let val;
	let oldId;
	const params = dependencies.map(d => (d._lock ? d._lock : d));
	const unlockableDeps = dependencies.map(d => d._unlock).filter(id);

	// Compute new value, call subscribers if changed.
	const accessor = function _computed() {
		if (isDirty) {
			const newVal = fn.apply(null, params.map(call));
			isDirty = false;
			const newId = hash(newVal);
			if (oldId !== newId) {
				oldId = newId;
				val = newVal;
				accessor._fire(val);
			}
			unlockableDeps.forEach(call);
		}
		return val;
	};

	// Add child nodes to the logic graph (value-based)
	Object.assign(accessor, subscribable());

	// Receive dirty flag from parent logic node (dependency).  Pass it down.
	accessor._setDirty = function setDirty() {
		if (!isDirty) {
			isDirty = true;
			accessor._forEachSubscription(s => s._setDirty && s._setDirty());
		}
		return accessor._hasSubscribers() && accessor;
	};

	// Remove this node from the logic graph completely
	accessor.detach = () => {
		accessor.unsubscribeAll();
		dependentSubscriptions.forEach(call);
	};

	const dependentSubscriptions = dependencies.map(d => d.subscribe(accessor._setDirty));

	return accessor;
};

export const computed = hashableComputed(id);
