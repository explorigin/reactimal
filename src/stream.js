import { id, call } from './util.js';
import { subscribable } from './subscribable.js';


export const hashableStream = hash => (fn, dependencies = []) => {
	let subscribers = [];
	let isDirty = true;
	let val;
	let oldId;
	const params = dependencies.map(d => (d._lock ? d._lock : d));
	const unlockableDeps = dependencies.map(d => d._unlock).filter(id);
	let currentProcess;

	// Compute new value, call subscribers if changed.
	const accessor = function _stream() {
		if (!isDirty) {
			return Promise.resolve(val);
		}
		if (!currentProcess) {
			currentProcess = Promise.all(params.map(call))
				.then(params => fn.apply(null, params))
				.then(res => {
					const newId = hash(res);
					isDirty = false;
					if (oldId !== newId) {
						oldId = newId;
						val = res;
						accessor._fire(val);
					}
					unlockableDeps.forEach(call);
					return val;
				})
				.finally(_ => {
					isDirty = false;
					currentProcess = null;
				});
		}
		return currentProcess;
	};

	// Add child nodes to the logic graph (value-based)
	Object.assign(accessor, subscribable());

	// Receive dirty flag from parent logic node (dependency).  Pass it down.
	accessor._setDirty = function setDirty() {
		if (!isDirty) {
			isDirty = true;
			accessor._forEachSubscription(s => s._setDirty && s._setDirty());
		}
		return accessor._hasSubscribers() && accessor;
	};

	// Remove this node from the logic graph completely
	accessor.detach = () => {
		accessor.unsubscribeAll();
		dependentSubscriptions.forEach(call);
	};

	const dependentSubscriptions = dependencies.map(d => d.subscribe(accessor._setDirty));

	return accessor;
};

export const stream = hashableStream(id);
