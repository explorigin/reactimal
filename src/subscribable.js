import { call } from './util.js';

export const subscribable = () => {
	const subscriptions = [];

	return {
		subscribe(fn) {
			subscriptions.push(fn);
			return () => {
				const idx = subscriptions.indexOf(fn);
				if (idx !== -1) {
					subscriptions.splice(idx, 1);
				}
				return subscriptions.length;
			};
		},
		unsubscribeAll() {
			subscriptions.splice(0, Infinity);
		},
		_fire(val) {
			return subscriptions.map(s => s(val)).forEach(call);
		},
		_forEachSubscription(fn) {
			subscriptions.forEach(fn);
		},
		_hasSubscribers() {
			return subscriptions.length > 0;
		}
	};
}
