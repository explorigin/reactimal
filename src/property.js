import { id } from './util.js';
import { subscribable } from './subscribable.js';


export const hashableProperty = hash => store => {
	let subscribers = [];
	let oldId = hash(store);
	let lockCount = 0;

	const accessor = function _prop(newVal) {
		const newId = hash(newVal);
		if (newVal !== undefined && oldId !== newId) {
			store = newVal;
			if (!lockCount) {
				oldId = newId;
				accessor._fire(store);
			}
		}
		return store;
	};
	// Add child nodes to the logic graph (value-based)
	Object.assign(accessor, subscribable());
	accessor._lock = () => {
		lockCount += 1;
		return accessor();
	};
	accessor._unlock = () => {
		if (lockCount && --lockCount === 0) {
			accessor(store);
		}
	};

	return accessor;
};

export const prop = hashableProperty(id);
